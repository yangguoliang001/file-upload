

(() => {
  const upload_btn = document.querySelector('.upload_btn');
  upload_btn.addEventListener('click', async () => {
    const data = await instance.post('/download', {
      filename: 'ddba517bc43a3faaad5bf2a9f39957b9.jpg'
    }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      responseType: 'arraybuffer' // 设置请求数据的类型为buffer类型
    })
    // 1.转成 blob 类型
    const blob = new Blob([data], { type: 'image/png' })
    // 2.创建一个a标签
    const a = document.createElement('a')
    a.download = 'hello.png' // 下载同源的文件
    a.rel = 'noopener'
    a.href = URL.createObjectURL(blob)
    // 3.点击
    a.dispatchEvent(new MouseEvent('click'))
    // 4.释放内存
    URL.revokeObjectURL(blob)
  })

})()
